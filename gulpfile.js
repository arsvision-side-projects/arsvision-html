'use strict';

var gulp        = require('gulp'),
    path        = require('path'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    newer       = require('gulp-newer'),
    notify      = require('gulp-notify'),
    plumber     = require('gulp-plumber'),
    rigger      = require('gulp-rigger'),
    rimraf      = require('rimraf'),
    sass        = require('gulp-sass'),
    slim        = require('gulp-slim'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;

var src = 'app/';
var build = 'dist/';

var config = {
  build: {
    html: build,
    css: build + 'css/',
    js: build + 'js/',
    img: build + 'img/',
    fonts: build + 'fonts/',
    images: build + 'public/'
  },
  src: { 
    html: src + '*.slim',
    style: src + 'sass/**/*.{scss,sass}',
    sass: src + 'sass',
    js: src + 'js/*.js',
    img: src + 'img/**/*.*',
    fonts: src + 'fonts/**/*.*',
    images: src + 'public/**/*.*'
  },
  watch: {
    html: src + '**/*.slim',
    style: src + 'sass/**/*.{scss,sass}',
    js: src + 'js/**/*.js',
    img: src + 'img/**/*.*',
    fonts: src + 'fonts/**/*.*',
    images: src + 'public/**/*.*'
  },
  clean: build,
  bower: 'bower_components/'
};

gulp.task('webserver', function () {
  return browserSync({
    server: {baseDir: build},
    host: 'localhost',
    port: 9000
  });
});

gulp.task('build:html', function() {
  gulp.src(config.src.html)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(slim({pretty: true}))
    .pipe(gulp.dest(config.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('build:css', function () {
  gulp.src(config.src.style)
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) // nested, expanded, compact, or compressed
    .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false}))
    .pipe(gulp.dest(config.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('build:js', function() {
  gulp.src(config.src.js)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest(config.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('build:image', function() {
  gulp.src(config.src.img)
    .pipe(newer(config.build.img))
    .pipe(imagemin({
      optimizationLevel: 5,
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest(config.build.img))
    .pipe(reload({stream: true}));
});

gulp.task('build:fonts', function() {
  gulp.src(config.src.fonts)
    .pipe(gulp.dest(config.build.fonts))
    .pipe(reload({stream: true}));
});

gulp.task('build:public', function() {
  gulp.src(config.src.images)
    .pipe(gulp.dest(config.build.images))
    .pipe(reload({stream: true}));
});

gulp.task('clean', function(cb) {
  rimraf(config.clean, cb);
});

gulp.task('build', [
  'build:html',
  'build:css',
  'build:js',
  'build:image',
  'build:fonts',
  'build:public'
]);

gulp.task('watch', function() {
  gulp.watch(config.watch.html, ['build:html']);
  gulp.watch(config.watch.style, ['build:css']);
  gulp.watch(config.watch.js, ['build:js']);
  gulp.watch(config.watch.img, ['build:image']);
  gulp.watch(config.watch.fonts, ['build:fonts']);
  gulp.watch(config.watch.images, ['build:public']);
});

gulp.task('default', ['build', 'webserver', 'watch']);