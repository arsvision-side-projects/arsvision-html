$(function() {
  if($('.imgLiquidFill').length){$('.imgLiquidFill').imgLiquid({fill:true});};
  if($('.welcome-slider').length){
    $(window).on('load resize',function(e){var i = $(window).height(); if($(window).width()<768) {i-=90;} else {i-=150;}; $('.welcome-slider .swiper-slide').css('height', i);});
    var swiper1 = new Swiper('#slider-1',{nextButton:'#slider-1 .swiper-button-next',prevButton:'#slider-1 .swiper-button-prev',loop:true,autoplay:4200});
  };
  if($('#slider-2').length){var swiper2 = new Swiper('#slider-2',{nextButton:'.news .swiper-button-next',prevButton:'.news .swiper-button-prev',loop:true,autoplay:4200});
  };
  $('.nav-toggle').click(function() {$('.sidebar, .sidebar-content').addClass('active'); $('body').addClass('noscroll');});
  $('.nav-close').click(function() {$('.sidebar-content').removeClass('active'); setTimeout(function() {$('.sidebar').removeClass('active'); $('body').removeClass('noscroll');}, 400);});
  if($('.wow').length){wow = new WOW({animateClass:'animated', offset:100, callback:function(box){}}); wow.init();};
  
  
  function initVimeo(){
    $('.vimeoFrame').each(function(){

        var $wrap = $(this),
            tTimer,
            $vid = $wrap.find('iframe')
            zp=$f($vid[0]),
            $pe=$wrap.find('.zPlayButton-wrap');
   $pm=$wrap.find('.zMuteButton-wrap');
        zp.addEvent('ready', function(){
            zp.api('play');
            zp.api('pause');
            zp.api('unload');
            zp.api('getVideoHeight',function(VH){
                zp.api('getVideoWidth',function(VW){
                    var dim = VH/VW;
                    if ($wrap.data('bgsize')=='cover'){
                        $(window).resize(function(){
                            clearInterval(tTimer);
                            tTimer = setTimeout(function(){
                                var WW = $wrap.width(),WH = $wrap.height();
                                if (WH/WW<dim)
                                    $vid.css({'height':WH+1000,'width':WW,'margin':'-500px 0'})
                                else {
                                    $vid.css({'height':WH+1000,'width':Math.round(WH/dim),'margin':'-500px -'+Math.round((WH/dim-WW)/2)+'px'});
                                }
                            },100);
                        })

                    }
                    else{
                        $(window).resize(function(){
                            clearInterval(tTimer);
                            tTimer = setTimeout(function(){
                                $vid.height(Math.round($wrap.width()*dim)+400);
                            },100)
                        })
                    }
                    $(window).resize();
                })
            })
            $pe.click(function(){
                zp.api('paused', function (value) {
                    if (value) {
                        zp.api('getCurrentTime', function (value) {
                            if (value>29) zp.api('seekTo', 0);
                            $pe.css('opacity',0);
                            zp.api('play');
                        });
                    }
                    else zp.api('pause');
                });
            })
   //-------off sound video
   $pm.click(function(){
    if($(this).children().hasClass('mute-active') == false){
     $(this).children().addClass('mute-active');
     zp.api('setVolume', 0);
    } else { $(this).children().removeClass('mute-active'); 
      zp.api('setVolume', 1);  };  
   });
   //-------------
            zp.addEvent('playProgress', function(data) {
                $wrap.closest('.fadeSlider').slick('slickPause');
                $wrap.closest('.fadeSlider').find('.slick-dots').fadeOut(500);
                if (data.seconds>29.5){
                    zp.api('pause');
                }
            });
            zp.addEvent('play',function(){
    $('.topSliderWrap').addClass('playing');
                $wrap.closest('.fadeSlider').slick('slickPause');
                $wrap.closest('.fadeSlider').find('.slick-dots').fadeOut(500);
                $pe.css('opacity',0);
            })
            zp.addEvent('pause',function(){
    $('.topSliderWrap').removeClass('playing');
                $wrap.closest('.fadeSlider').slick('slickPlay');
                $wrap.closest('.fadeSlider').find('.slick-dots').fadeIn(500);
                $pe.css('opacity',1);
            })

        })

    })
  }
});