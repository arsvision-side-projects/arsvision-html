function initMap() {
  var mapDiv = document.getElementById('google-map');
  var coords = { lat: 52.4449519, lng: 16.8783243 };
  var stylez = [{
    featureType: "all",
    elementType: "all",
    stylers: [{saturation: -100}]
  }];
  var map = new google.maps.Map(mapDiv, {
    zoom: 16,
    scrollwheel: false,
    disableDefaultUI: true,
    mapTypeControl: true,
    zoomControl: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.MEDIUM,
      position: google.maps.ControlPosition.LEFT_TOP
    },
    mapTypeControlOptions: {mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'tehgrayz']},
    center: coords
  });
  var mapType = new google.maps.StyledMapType(stylez, {name:"Grayscale"});    
  map.mapTypes.set('tehgrayz', mapType);
  map.setMapTypeId('tehgrayz');
  var image = 'img/marker.png';
  var marker = new google.maps.Marker({
    draggable: false,
    animation: google.maps.Animation.DROP,
    icon: image,
    map: map,
    position: coords
  });
}